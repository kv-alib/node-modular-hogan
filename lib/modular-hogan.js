var fs = require('fs');
var paths = require('path');
var ctx = {};

module.exports = function (templates, partials, Hogan) {
  if (typeof Hogan === 'undefined' ||
    typeof Hogan.scan === 'undefined' ||
    typeof Hogan.parse === 'undefined' ||
    typeof Hogan.generate === 'undefined') {
    throw new Error('please, require Hogan!');
  }

  Hogan.fcache = {};

  Hogan.fcompile = function (template, options) {
    options = options || {};
    options.filename = template.path;

    var key = template.path + ':string';

    if (options.cache && Hogan.fcache[key]) {
      return Hogan.fcache[key];
    }

    var tpl = getTemplate(template);

    try { 
      var rt = Hogan.generate(
        Hogan.parse(
          Hogan.scan(tpl, options.delimiters), 
          tpl, options), 
        tpl, options);

      return options.cache ? Hogan.fcache[key] = rt : rt;
    } catch (error) {
      throw new Error('Error reading template file ' + template.name + '/' + template.path + ': ' + error.message);
    }
  };
  
  var getTemplate = function(template) {
    var tpl = templates[template.name];
    if ((tpl === undefined) || (tpl === null)) {
      tpl = partials[template.name];
    }
    if ((tpl === undefined) || (tpl === null)) {
      if (fs.existsSync(template.path)) {
        return fs.readFileSync(template.path, 'utf8');
      } else {
        throw new Error('no template ' + template.name + ' in app.templates or app.partials, nor at path ' + template.path);
      }
    } else {
      return tpl.data;
    }
  };

  var renderPartials = function(partials, opt) {
    var name, result;
    result = {};
    for (name in partials) {
      var partial = partials[name];
      if (typeof partial === 'string') {
        if (!paths.extname(partial)) {
          partial += ctx.ext;
        }
        partial = ctx.lookup(partial);
        result[name] = Hogan.fcompile({
          name: name,
          path: partial
        }, opt);
      } else {
        result[name] = Hogan.fcompile(partial, opt);
      }
    }
    return result;
  };

  Hogan.renderFile = function (template, options, fn) {
    ctx = this;
    try {
      var partials = renderPartials(options.partials, options);
      var tpl = (template.name === undefined) ? { name:template, path:template } : template;

      fn(null, Hogan.fcompile(tpl, options).render(options, partials));
    } catch (error) {
      fn(error);
    }
  };

  Hogan.__express = Hogan.renderFile;

  return Hogan;
};