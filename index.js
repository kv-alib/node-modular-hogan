module.exports = function(app) {
  if (!app.templates) app.templates = {};
  if (!app.partials) app.partials = {};
  
  // override the lookup method of express's View class
  // to allow templates to be sourced from somewhere other
  // than the filesystem of the master project
  var lookup_delegate = app.get('view').prototype.lookup;
  app.get('view').prototype.lookup = function(path){
    // module templates and partials can be overridden by 
    // the master project by supplying a template with the same name
    var maybeErr;
    try {
      var result = lookup_delegate.apply(this, arguments);
      if (result)
        return result;
    } catch (err) {
      maybeErr = err;
    }
    if (app.templates[path])
      return app.templates[path];
    if (app.partials[path])
      return app.partials[path]; 
    throw (maybeErr) ? maybeErr : new Error('no template found for ' + path);
  };
  
  // return an instance of the templating engine
  return require('./lib/modular-hogan.js')(app.templates, app.partials, require('hogan.js')).__express;
}